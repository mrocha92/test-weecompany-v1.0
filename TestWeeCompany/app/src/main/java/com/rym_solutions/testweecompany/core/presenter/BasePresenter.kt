package com.rym_solutions.testweecompany.core.presenter

import BaseViewModel


interface BasePresenter {

    fun attachView(baseViewModel: BaseViewModel)
    fun deattachView()
}
