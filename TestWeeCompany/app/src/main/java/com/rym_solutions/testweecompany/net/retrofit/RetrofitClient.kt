package com.rym_solutions.testweecompany.net.retrofit

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    private var instance: Retrofit?= null
    val getInstance:Retrofit
        get() {
            if (instance == null)
                instance = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl("http://demo.weefusion.net/desarrollo/weepatient/API/api/Utilidades")
                    .client(OkHttpClient.Builder().build())
                    .build()
            return instance!!
        }
}