package com.rym_solutions.testweecompany.ui.country.view

import BaseViewModel
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rym_solutions.testweecompany.R
import com.rym_solutions.testweecompany.net.models.CountryRequest
import com.rym_solutions.testweecompany.ui.country.presentation.CountryPresenter
import kotlinx.android.synthetic.main.fragment_state.*
import javax.inject.Inject

class CountryFragment : Fragment(), BaseViewModel {

@Inject

lateinit var countryPresenter: CountryPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_state, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        countryPresenter.attachView(this)
        //TODO  show Progress view
/*
        countryPresenter.getCountry(CountryRequest("")){ countryList ->
            rvRecycler.adapter = CountryAdapter(countryList) { item ->
                openDetail(item)

            }
            //  TODO hide Progress view


        }
*/
    }

    private fun openDetail(name: String) {


    }

    override fun showProgress() {

    }

    override fun hideProgress() {
        TODO("Not yet implemented")
    }

}