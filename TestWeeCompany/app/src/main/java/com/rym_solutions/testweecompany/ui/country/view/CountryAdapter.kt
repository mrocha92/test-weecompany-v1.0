package com.rym_solutions.testweecompany.ui.country.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rym_solutions.testweecompany.R
import kotlinx.android.synthetic.main.item_state.view.*
import kotlin.properties.Delegates


class CountryAdapter(items: List<String> = emptyList(), val listener: (String) -> Unit) :
    RecyclerView.Adapter<CountryAdapter.ViewHolder>() {

    var items: List<String> by Delegates.observable(items) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_state, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
        holder.itemView.setOnClickListener {
            listener(item)
        }

    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: String) = with(itemView) {
            textState.text = item
        }
    }
}