package com.rym_solutions.testweecompany.net.retrofit

import com.rym_solutions.testweecompany.net.models.CountryRequest
import com.rym_solutions.testweecompany.net.models.CountryResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST


interface RequestInterface {
    @POST("/Pais_GetPais")
    fun getEstablishments(
        @Body countryRequestModel: CountryRequest
    ):Observable <CountryResponse>


}