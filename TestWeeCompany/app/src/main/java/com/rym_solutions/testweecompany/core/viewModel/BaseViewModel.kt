

interface BaseViewModel {
    fun showProgress()
    fun hideProgress()
}