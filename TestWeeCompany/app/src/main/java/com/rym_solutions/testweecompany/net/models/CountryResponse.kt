package com.rym_solutions.testweecompany.net.models

import com.google.gson.annotations.SerializedName

data class CountryResponse(
    @SerializedName("isOnline") private val isOnline: Boolean?,
    @SerializedName("dsRespuesta") val paises: List<Pais>?
)

data class Pais(
    @SerializedName("idPais") val idPais: String?,
    @SerializedName("Pais") val pais: String?
)
