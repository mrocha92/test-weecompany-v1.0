package com.rym_solutions.testweecompany.ui.country.presentation

import com.rym_solutions.testweecompany.core.presenter.BasePresenter
import com.rym_solutions.testweecompany.net.models.CountryRequest
import com.rym_solutions.testweecompany.net.models.Pais

interface CountryPresenter: BasePresenter {
    fun getCountry(paramter: CountryRequest, callback: List<Pais>)
}