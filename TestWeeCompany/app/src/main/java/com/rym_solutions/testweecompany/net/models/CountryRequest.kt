package com.rym_solutions.testweecompany.net.models

import com.google.gson.annotations.SerializedName

data class CountryRequest (
    @SerializedName("parametro") var parametro: String = ""
)